$(document).ready(function () {
    var image = $('img');

    image.mapster(
        {
            fillOpacity: 0.4,
            fillColor: "676262",
            strokeColor: "252424",
            strokeOpacity: 0.8,
            strokeWidth: 4,
            stroke: true,
            isSelectable: true,
            singleSelect: true,
            mapKey: 'name',
            listKey: 'name',
            onClick: function (e) {
                console.log($(this).data('id'));
            }

        });
});