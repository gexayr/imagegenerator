<?php
/*
 * Easy Imagemap Generator - Free Online Imagemapping Tool
 * build with
 *		-	PHP Logic
 *		-	jQuery Maphighlight
 *		-	CSS3 Layout
 *		-	Flash Uploadify
 *		-	HTML5 Upload
 * Date 	2012 for private use and since January 2013 for public
 * Version	v2.0
 * About	http://dariodomi.de
 * Copyright (c) by Dario D. Müller
 * GitHub -> https://github.com/DarioDomiDE/Imagemap-Generator
 */

require_once('config.php');
session_start();

?><!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	
	<!-- Copyright (c) 2014 by Dario D. Müller -->
	<!-- All rights reserved  -->
	<!-- Alle Rechte vorbehalten -->
	<title>Easy Imagemap Generator</title>
	
	<!-- Icon -->
	<link rel="shortcut icon" href="favicon.ico" />
	
	<!-- SEO -->
	<meta name="author" content="Dario D. Müller" />
	<meta name="publisher" content="http://dariodomi.de" />
	<meta name="copyright" content="(c) 2014 Dario D. Müller" />
	<meta name="page-topic" content="imagemap generator, html imagemaps, generator, html, map area generator, area imagemaps, links into images, links in images" />
	<meta name="page-type" content="html, imagemap, area, map, coordinate, generator, links, image" />
	<meta name="description" content="Easy Imagemap Generator for html image mapping. Select an image. Set links and clickable areas to your image. Get HTML code for Imagemaps." />
	<meta name="keywords" content="html, imagemap, generator, map, area, imagemap generator, creating map areas, html infos, newsletter" />
	
	<!-- Author -->
	<link rel="author" href="https://plus.google.com/113304109683958874741/" />
	<meta property="article:author" content="https://plus.google.com/113304109683958874741/" />
	<meta name="Author" content="Dario D. Müller">

    <input type="hidden" id="last_data_id" value="3">

    <!-- JS -->
	<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="js/jquery.maphilight.min.js"></script>
	<script type="text/javascript" src="js/jquery.uploadify-3.1.min.js"></script>
	<script type="text/javascript" src="js/script2.js"></script>
	
	<!-- CSS -->
	<link rel="stylesheet" href="css/style2.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/snippet.css" type="text/css" media="screen" />
	
</head>

<?php

if(!is_dir($uploadDir))
{
	mkdir($uploadDir);
}

// delete old images from server -> 2 = 1 day
$lifetimeInDays = 2;
if ($handle = opendir($uploadDir))
{
	while (false !== ($file = readdir($handle)))
	{
		if($file != '.' && $file != '..')
		{
			$name = explode('_', $file);
			$dateStr = $name[0];
			$year = substr($dateStr, 0, 2);
			$month = substr($dateStr, 2, 2);
			$day = substr($dateStr, 4, 2);
			$date = new DateTime('20'.$year.'-'.$month.'-'.$day);
			$dateNow = new DateTime(date('Y-m-d', time() - 60*60*24*$lifetimeInDays));
			$interval = $date->diff($dateNow);
			$diff = (int)$interval->format('%R%a');
			if($diff >= 0)
			{
				// file is too old -> delete
				unlink($uploadDir.'/'.$file);
			}
		}
	}
	closedir($handle);
}

// Check Session set and Loading previous Image
$uploaded = false;
if(isset($_SESSION['image']) && $_SESSION['image'] != null && !empty($_SESSION['image'][0]) && substr_count($_SESSION['image'][0], '/') >= 1) {
	if(file_exists($_SESSION['image'][0]))
	{
		$uploaded = true;
	}
}

?>

<body>
	<header>
		<div id="header">

		</div>
	</header>
		<?php
			if($uploaded)
				echo '<div id="navi" currentValue="#imagemap4posis">';
			else
				echo '<div id="navi" currentValue="#upload">';
		?>
		<ul>
			<li style="width: 600px"><a href="#" rel="#upload" class="blue">Imagemap Generator</a></li>
		</ul>
	</div>
	
	<div id="upload" class="effect infobox">
		<article>
			<div class="uploadContainer infobox2">
				<div id="uploadUndo"<?php if(!$uploaded) echo ' class="hidden"'; ?>></div>

				<!-- HTML5 Drag End -->
				<form id="uploadForm" method="post" action="upload.php?v2" enctype="multipart/form-data">
					<div id="drop">
						<a>Image Upload <i class="icon icon-upload-2"></i></a>
						<input type="file" name="image" multiple />
					</div>
					<div id="uploadProcess"><!-- The file uploads will be shown here --></div>
				</form>
				
				<!-- Flash Upload Begin -->
				<form action="#" method="post" id="flashUpload" class="hidden">
					<input type="file" name="image" id="uploadify" class="hidden" />
					<script type="text/javascript">
						$(function() {
							<?php $timestamp = time(); ?>
							$('#uploadify').uploadify({
								'formData'      : {
									'timestamp' : '<?php echo $timestamp; ?>',
									'<?php echo session_name();?>' : '<?php echo session_id();?>'
								},
								'fileObjName'   : 'image',
								'fileTypeExts'  : '*.jpg; *.jpeg; *.gif; *.png; *.bmp, *tif, *tiff', 
								'buttonText'	: 'Image Upload (Flash) <i class="icon icon-upload-2"></i>',
								'swf'           : 'uploadify.swf',
								'uploader'      : 'upload.php',
								'width'			: 220,
								'height'		: 35,
								'onUploadSuccess'	: function(file, dataAuth, response) {
									
									var json = $.parseJSON(dataAuth);
									if(json['status'] == 'success') {
									
										// set upload to session
										jQuery.ajax({
											type: 'POST',
											url: 'upload_ident.php', 
											data: {'file': json['file'], 'width': json['width'], 'height': json['height']},
											dataType : 'json'
										});
										// hide upload area and show imagemap generator
										$('#imagemap4posis #mapContainer').find('img').attr('src', json['file']);
										//if(navigator.appName.indexOf("Internet Explorer") != -1 || navigator.userAgent.toLowerCase().indexOf('msie') != -1)
											//$('#imagemap4posis #mapContainer').find('img').attr('width', '').attr('height', '');
										
										removeErrorMessage();
										removeOldMapAndValues();
										$('#navi').attr('currentValue', '#imagemap4posis');
										
										$('#upload').slideUp(400, function() {
											$('#uploadUndo, #uploadUndo2').show();
											$('#imagemap4posis').slideDown(400, function() {
												resizeHtml();
											});
											loadImagemapGenerator(json['width'], json['height']);
										});
									} else if(json['status'] == 'error') {
										alert(json['message']);
									}
								}
								
							});
							if($('#uploadify-button').length == 0)
								$('#flashUploadSwitch').hide();
						});
					</script>
				</form>
				
				<!-- Switch between Flash and HTML5 Upload -->
				<p><a href="#" id="flashUploadSwitch">Switch to <span class="flash">Flash</span><span class="html5 hidden">HTML5</span> Upload</a></p>
					
				<p id="uploadUndo2"<?php if(!$uploaded) echo ' class="hidden"'; ?>>Back to my image <i class="icon icon-back"></i></p>
				<p class="headline">Or insert an image link</p>
				<form action="#" id="linkform">
					<input type="text" name="fileurl" value="" placeholder="http://www..." id="imageurl" class="insetEffect" />
					<a href="#" class="imageurl_submit"></a>
				</form>
			</div>
		</article>
	</div>



	<div id="imagemap4posis">
		<div id="newUpload"><span></span></div>
		<div id="urlMessage"><p class="effect">You can't see an image?<br /><a href="#">Please upload a new one &raquo;</a></p></div>
		<div id="mapContainer" class="effect">
			<?php
				$attr = '';
				if($uploaded && $_SESSION['image'][1] != 0 && $_SESSION['image'][2] != 0)
				{
					//$attr = ' width="'.$_SESSION['image'][1].'" height="'.$_SESSION['image'][2].'"';
				}
			?>
			<img src="<?php echo ($uploaded) ? $_SESSION['image'][0] : '#'; ?>"<?php echo $attr; ?> id="main" class="imgmapMainImage" alt="" usemap="#map" />
			<map name="map" id="map"></map>
		</div>


		<div class="form">
			<p>Click into the image to set coordinate values :)</p>
			<div id="clearStyleButtons">
				<div class="effect clearButton"><i class="icon icon-add"></i> Add Area</div>
				<div class="effect clearCurrentButton"><i class="icon icon-clear"></i> Clear Last</div>
				<div class="effect clearAllButton"><i class="icon icon-clear"></i> Clear All</div>
				<div class="effect textareaButton3"><i class="icon icon-upload"></i> Change Image</div>
			</div>
			<input id="coordsText" class="effect" name="" type="text" value="" placeholder="&laquo; Coordinates &raquo;" />
			<textarea name="" id="areaText" class="effect" placeholder="&laquo; HTML-Code &raquo;"></textarea>
		</div>

        <a class="apply" href="#">Apply</a>
    </div>
	<div id="dots"></div>

	<footer>

	</footer>
	
	<!-- jQuery File Upload -->
	<script src="js/jquery.ui.widget.js"></script>
	<script src="js/jquery.iframe-transport.js"></script>
	<script src="js/jquery.fileupload.js"></script>
	<script src="js/js.cookie-2.1.4.min.js"></script>
	<script src="js/script_upload.js"></script>
	
	<script type="text/javascript">
		/* init */
		$(function() {
			<?php if($uploaded) { ?>
				setTimeout(function() {
					$('#imagemap4posis').slideDown(400, function() {
						resizeHtml();
					});
					loadImagemapGenerator(0,0);
				}, 600);
			<?php } else { ?>
				$('#upload').delay(600).slideDown(400, function() {
					resizeHtml();
				});
				resizeHtml();
			<?php } ?>
		});
	</script>


</body>
</html>